import datetime
from decimal import Decimal

from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from lfs.order.settings import ORDER_STATES, SUBMITTED

class StorageManager(models.Manager):
    def __init__(self):
        super(StorageManager, self).__init__()
        self._db = 'shop_maintenance'

    def next_number(self):
        try:
            current_val = self.latest('created').number
        except ProductOrder.DoesNotExist:
            current_val = 0
        try:
            next_val = int(current_val) + 1
        except ValueError:
            next_val = 1
        return "%07d" % next_val


class ProductCondition(models.Model):
    sku = models.CharField(
        _(u"SKU"), max_length=200,
        help_text=_(u"Your unique article number of the product.")
    )
    stock_amount = models.FloatField(_(u"Stock amount"), default=0)
    stock_amount_diff = models.FloatField(_(u"Stock amount diff"), default=0)
    updated = models.DateTimeField(
        _(u"Update date/time"),
        default=datetime.datetime.now
    )
    updated_by = models.CharField(_(u"Updated by shop"), max_length=50)

    def __unicode__(self):
        return u"%s: %s" % (self.sku, self.stock_amount)
    
    objects = StorageManager()

    class Meta:
        managed = False
        db_table = 'storage_maintenance_productcondition'


class ProductOrder(models.Model):
    number = models.CharField(max_length=30)
    user = models.CharField(_(u"User"), max_length=255)

    created = models.DateTimeField(_(u"Created"), auto_now_add=True)
    created_by = models.CharField(_(u"Updated by shop"), max_length=50)

    modified = models.DateTimeField(_(u"Modified"), auto_now_add=True)
    state = models.PositiveSmallIntegerField(_(u"State"), choices=ORDER_STATES, default=SUBMITTED)

    price = models.FloatField(_(u"Price"), default=0.0)
    price_net = models.DecimalField(_(u"Price net"),  max_digits=10, decimal_places=2, default=Decimal(0))
    tax = models.FloatField(_(u"Tax"), default=0.0)

    customer_firstname = models.CharField(_(u"firstname"), max_length=50)
    customer_lastname = models.CharField(_(u"lastname"), max_length=50)
    customer_email = models.CharField(_(u"email"), max_length=50)

    nip = models.CharField(_(u"NIP"), max_length=13, default='')
    is_corporate = models.BooleanField(_(u"Is corporate"), default=False)

    invoice_firstname = models.CharField(_(u"Invoice firstname"), max_length=50)
    invoice_lastname = models.CharField(_(u"Invoice lastname"), max_length=50)
    invoice_company_name = models.CharField(_(u"Invoice company name"), null=True, blank=True, max_length=100)
    invoice_line1 = models.CharField(_(u"Invoice Line 1"), null=True, blank=True, max_length=100)
    invoice_line2 = models.CharField(_(u"Invoice Line 2"), null=True, blank=True, max_length=100)
    invoice_city = models.CharField(_(u"Invoice City"), null=True, blank=True, max_length=100)
    invoice_state = models.CharField(_(u"Invoice State"), null=True, blank=True, max_length=100)
    invoice_code = models.CharField(_(u"Invoice Postal Code"), null=True, blank=True, max_length=100)
    invoice_country = models.CharField(_(u"Invoice Country"), blank=True, null=True, max_length=100)
    invoice_phone = models.CharField(_(u"Invoice phone"), blank=True, max_length=20)

    shipping_firstname = models.CharField(_(u"Shipping firstname"), max_length=50)
    shipping_lastname = models.CharField(_(u"Shipping lastname"), max_length=50)
    shipping_company_name = models.CharField(_(u"shipping company name"), null=True, blank=True, max_length=100)
    shipping_line1 = models.CharField(_(u"Shipping Line 1"), null=True, blank=True, max_length=100)
    shipping_line2 = models.CharField(_(u"Shipping Line 2"), null=True, blank=True, max_length=100)
    shipping_city = models.CharField(_(u"Shipping City"), null=True, blank=True, max_length=100)
    shipping_state = models.CharField(_(u"Shipping State"), null=True, blank=True, max_length=100)
    shipping_code = models.CharField(_(u"Shipping Postal Code"), null=True, blank=True, max_length=100)
    shipping_country = models.CharField(_(u"Shipping Country"), blank=True, null=True, max_length=100)
    shipping_phone = models.CharField(_(u"Shipping phone"), blank=True, max_length=20)

    shipping_method = models.CharField(_(u"Shipping method"), blank=True, max_length=100)
    shipping_price = models.FloatField(_(u"Shipping Price"), default=0.0)
    shipping_tax = models.FloatField(_(u"Shipping Tax"), default=0.0)
    shipping_extra_info = models.TextField(_(u'Shipping extra information'), blank=True, default='')

    payment_method = models.CharField(_(u"Payment method"), blank=True, max_length=100)
    payment_price = models.FloatField(_(u"Payment Price"), default=0.0)
    payment_tax = models.FloatField(_(u"Payment Tax"), default=0.0)

    account_number = models.CharField(_(u"Account number"), blank=True, max_length=30)
    bank_identification_code = models.CharField(_(u"Bank identication code"), blank=True, max_length=30)
    bank_name = models.CharField(_(u"Bank name"), blank=True, max_length=100)
    depositor = models.CharField(_(u"Depositor"), blank=True, max_length=100)

    voucher_number = models.CharField(_(u"Voucher number"), blank=True, max_length=100)
    voucher_price = models.FloatField(_(u"Voucher value"), default=0.0)
    voucher_tax = models.FloatField(_(u"Voucher tax"), default=0.0)
    
    message = models.TextField(_(u"Message"), blank=True)
    is_wholesale = models.BooleanField(_(u"Wholesale order"), default=False)

    paid_amount = models.DecimalField(
        _(u"Paid amount"), default=Decimal('0.0'), decimal_places=2,
        max_digits=10
    )
    paid_date = models.DateTimeField(_(u"Paid date"), blank=True, null=True, default=None)
    wholesale_customer_id = models.PositiveIntegerField(_('Wholesale customer'), null=True, default=None)
    discount = models.IntegerField(_("Discount"), null=True, default=0)
    discount_net = models.DecimalField(_(u"Discount net"), default=Decimal('0.0'), decimal_places=2,
        max_digits=10, null=True, blank=True)
    discount_tax_id = models.PositiveIntegerField(null=True, default=None)

    def __unicode__(self):
        return "%s (%s %s)" % (
            self.created and self.created.strftime("%x %X") or '',
            self.customer_firstname,
            self.customer_lastname
        )

    objects = StorageManager()

    class Meta:
        ordering = ("-created", )
        managed = False
        db_table = 'storage_maintenance_productorder'


class ProductOrderItem(models.Model):
    order = models.ForeignKey(ProductOrder, related_name="items")

    price_net = models.FloatField(_(u"Price net"), default=0.0)
    price_gross = models.FloatField(_(u"Price gross"), default=0.0)
    tax = models.FloatField(_(u"Tax"), default=0.0)

    product_amount = models.FloatField(_(u"Product quantity"), blank=True, null=True)
    product_sku = models.CharField(_(u"Product SKU"), blank=True, max_length=100)
    product_name = models.CharField(_(u"Product name"), blank=True, max_length=100)
    product_price_net = models.FloatField(_(u"Product price net"), default=0.0)
    product_price_gross = models.FloatField(_(u"Product price gross"), default=0.0)
    product_tax = models.FloatField(_(u"Product tax"), default=0.0)
    unit = models.IntegerField(_("Units"), default=0)
    discount = models.IntegerField(
        _("Discount"), default=0
    )

    def __unicode__(self):
        return "%s" % self.product_name#    

    objects = StorageManager()

    class Meta:
        managed = False
        db_table = 'storage_maintenance_productorderitem'
