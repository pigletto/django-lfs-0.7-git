# django imports
from decimal import Decimal

from django.contrib.sites.models import Site

from storage_maintenance.models import ProductOrder, ProductOrderItem


def create_storage_maintenance_order(order):
    """
    Connect to storage management and update order
    """
    site = Site.objects.get_current()
    shop_tag = site.domain

    is_corporate = False
    if order.invoice_company_name and order.invoice_company_name.strip() != '':
        is_corporate = True

    product_order = ProductOrder.objects.create(
        number = order.number,
        user = order.user and order.user.username or '',
        created_by = shop_tag,
        created = order.created,
        modified = order.state_modified,
        state = order.state,

        price=order.price,
        price_net=order.price - order.tax,
        tax=order.tax,
        nip='',
        is_corporate=is_corporate,

        customer_firstname=order.customer_firstname,
        customer_lastname=order.customer_lastname,
        customer_email=order.customer_email,

        shipping_method=order.shipping_method.__unicode__(),
        shipping_price=order.shipping_price,
        shipping_tax=order.shipping_tax,
        payment_method=order.payment_method.__unicode__(),
        payment_price=order.payment_price,
        payment_tax=order.payment_tax,

        invoice_firstname=order.invoice_firstname,
        invoice_lastname=order.invoice_lastname,
        invoice_company_name=order.invoice_company_name,
        invoice_line1=order.invoice_line1,
        invoice_line2=order.invoice_line2,
        invoice_city=order.invoice_city,
        invoice_state=order.invoice_state,
        invoice_code=order.invoice_code,
        invoice_country=order.invoice_country.__unicode__(),
        invoice_phone=order.invoice_phone,

        shipping_firstname=order.shipping_firstname,
        shipping_lastname=order.shipping_lastname,
        shipping_company_name=order.shipping_company_name,
        shipping_line1=order.shipping_line1,
        shipping_line2=order.shipping_line2,
        shipping_city=order.shipping_city,
        shipping_state=order.shipping_state,
        shipping_code=order.shipping_code,
        shipping_country=order.shipping_country.__unicode__(),
        shipping_phone=order.shipping_phone,
        shipping_extra_info='',

        account_number = order.account_number,
        bank_identification_code = order.bank_identification_code,
        bank_name = order.bank_name,
        depositor = order.depositor,

        voucher_number = order.voucher_number,
        voucher_price = order.voucher_price,
        voucher_tax = order.voucher_tax,
        message = order.message,
        is_wholesale = False,
        paid_amount = Decimal('0.0'),
        paid_date = None,
        wholesale_customer_id = None,
        discount = 0,
        discount_net = Decimal('0.0'),
        discount_tax_id = None
    )

    # bez ponizszej linii nie jest zapisywana poprawna data
    product_order.created = order.created
    product_order.save()

    for item in order.items.all():
        ProductOrderItem.objects.create(
            order = product_order,

            price_net = item.price_net,
            price_gross = item.price_gross,
            tax = item.tax,

            product_amount = item.product_amount,
            product_sku = item.product_sku,
            product_name = item.product_name,
            product_price_net = item.product_price_net,
            product_price_gross = item.product_price_gross,
            product_tax = item.product_tax
        )
