# -*- coding:utf-8 -*-

import datetime

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.sites.models import Site
from django.db.models import Q

from lfs.catalog.models import Product
from storage_maintenance.models import ProductCondition


class Command(BaseCommand):
    def handle(self, *args, **options):
        site = Site.objects.get_current()
        Product.objects.exclude(Q(sku='')|Q(sku__isnull=True)).update(manage_stock_amount=True)
        for product in Product.objects.exclude(Q(sku='')|Q(sku__isnull=True)):
            condition, created = ProductCondition.objects.get_or_create(
                sku=product.sku
            )
            if created:
                condition.stock_amount = product.stock_amount
                condition.updated = datetime.datetime.now()
                condition.updated_by = site.domain
                condition.save()
                print "CREATED %s with amount %s from shop %s" % (
                    product.sku, product.stock_amount, site.domain
                )
            else:
                if product.stock_amount != condition.stock_amount:
                    print (
                        u"Product SKU %s already registered and stock_amount "
                        u"differs. This shop (%s) vs (%s) Shop management. Updated to Shop management") % (
                            product.sku,
                            product.stock_amount,
                            condition.stock_amount
                        )
                    # aktualizacja stanu w aktualnym sklepie do stanu z magazynu centralnego
                    #z
                    product._force_notify_from_stock_management = True
                    product.save()

