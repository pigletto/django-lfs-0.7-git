# -*- coding:utf-8 -*-

import datetime

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.sites.models import Site
from storage_maintenance.models import ProductOrder, ProductOrderItem

class Command(BaseCommand):
    def handle(self, *args, **options):
        from lfs.order.models import Order
        from lfs.order.utils import create_storage_maintenance_order

        for order in  Order.objects.all():
            try:
                ProductOrder.objects.get(number=order.number)
                print u"Skipping %s - already registered" % order.number
            except ProductOrder.DoesNotExist:
                create_storage_maintenance_order(order)
                print u"CREATED %s" % order.number
